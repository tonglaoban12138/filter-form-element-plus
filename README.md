# filter-form

## 简单介绍

实际项目开发中会经常搭建 form,重复搭建,耗时耗力,开发一个库来重复使用,快速搭建一些常见的表单

## 目前支持组件

cascader input select select-v2 datePicker timePicker radio

后续会加入更多组件

## 使用方法

### 本示例为最简单的使用方法

```typescript
import FilterForm from 'filter-form-element-plus'
import { type IForm, type IFormConfigurationItems } from 'filter-form-element-plus' // js无需导入
```

### 准备好数据对象,和配置对象

#### 数据对象

```typescript
const formData: IForm = {
  data1: '',
  data2: ''
}
```

#### 配置对象

```typescript
const formConfig: IFormConfigurationItems = {
  elForm: {
    // element-plus[后面所有的配置项无特殊声明均为element-plus,不在赘述]form的所有属性
  },
  typeSetting: [
    [
      {
        vModel: 'data1', // 绑定值,需要与数据对象中的元素对应
        component: 'select', // 组件的类型
        col?: {}, // 可选,默认占满一行 适用col所有属性
        elFormItem?: {
          slotScope?:[
            { // 若您想使用插槽,则需使用该字段,适用el-form-item所有插槽
            slotName:'label',
            render: () => h()
          }
          ]
        }, // 可选,适用el-form-item的所有属性
        customClassName?: {
          elFormItemClassName?: ..., // 修改elFormItem级别的class css的预处理器生成string类型的类名,这里仅支持这种模式传入
          elModuleClassName?: ... // 修改elFormItem以下级别的class
        }, // 自定义组件样式
        elModule?: {
          slotScope?:[{ // 适用以上支持的组件el-module所有插槽
            slotName:'label',
            render: () => h()
          }]
        }, //可选,适用以上支持的组件el-module所有属性
        infinite?: {
          infiniteFn?: () => {}, // 执行函数,默认为() => ({})
          'infinite-scroll-disabled': false // 识别指令写法
        }, // 无限滚动 目前仅支持select,支持无限滚动所有指令
        elOption?: {
          options?: [], // 组件为select时option写在这里,支持select option所有属性
          radioOptions?: [ { label: 'radio1' }, { label: 'radio2' }], // 组件为radio时option写在这里,支持radio所有属性
          radioButtonOptions?: [] // 组件为radioButton时option写在这里,支持radioButton所有属性
        }
      },
      {
        vModel: data2,
        component: 'select',
        ...
      },
      {
        // 某些情况可能需要自定义一些稀奇古怪的东西,您可以直接render
        component: () => h('div',{},'稀奇古怪的东西'), // 必须是一个render函数
      }
    ],
    // 某些情况可能需要自定义,例如标题,或者其他稀奇古怪的东西,您可以直接render
    () => h('div', {}, '标题'), // 必须是一个render函数
    [...]
    ...
  ]
}
```

#### 特殊情况

您发现在 component 选项中还增加了一个 operation 选项

此选项可让您有些操作可以进入组件内部处理

比如: 最重要的表单验证

我将 ref 实例暴露了出来,让您可以自由操作

这样相比较于自定义写法更规范

```typescript
// from filter-form.vue
...
provide('formRef', formRef)
...
```

```typescript
...
{
    component:'operation',
    operationList?: [ () => h('div',{},'操作1'), () => h('div',{},'操作2'), ...]
}
```

### template 使用

```vue
<script setup lang="ts">
...
const getValueFn = (val: any) => {
  console.log(val) // 在此处即可获得处理后的值
}
...
</script>
<template>
  <FilterForm :filterConfig="formConfig" :filterForm="formData" @get-form-value="getValueFn" />
</template>
```
