const ImmunityKey = ['vModel', 'components', 'col']
const parameterProcessing = (props: any) => {
  const parameter: Record<string, any> = {}
  Object.keys(props).forEach(item => {
    if (ImmunityKey.includes(item)) return
    parameter[item] = props[item]
  })
  return parameter
}
export default parameterProcessing
