import { computed, defineComponent, h } from 'vue'
import parameterProcessing from '../helpers/parameterProcessing'
import {
  SelectV2Model,
  InputModel,
  Operation,
  SelectModel,
  CascaderModel,
  DatePickerModel,
  TimePickerModel,
  RadioModel,
  ColorPickerModel
} from '../components'
import type { ITypeSetting } from '../types'
import type { PropType } from 'vue'

const formTypeProcessing = defineComponent({
  name: 'FromTypeProcessing',
  props: {
    filterItem: {
      type: Object as PropType<ITypeSetting>,
      default: () => ({})
    },
    filterElement: {
      type: [String, Number, Boolean, Date, Array],
      default: ''
    }
  },
  emits: ['get-val'],
  setup(props, context) {
    const element = computed(() => props.filterElement)
    const modeTypeChooseFn = (arr: any) => {
      const hData = () => {
        return {
          value: element.value,
          attributes: parameterProcessing(arr),
          onPushVal: (val: string) => {
            context.emit('get-val', val)
          }
        }
      }
      const modeType = {
        cascader: () => h(CascaderModel, hData()),
        colorPicker: () => h(ColorPickerModel, hData()),
        dataPicker: () => h(DatePickerModel, hData()),
        operation: () => h(Operation, { attributes: parameterProcessing(arr) }),
        input: () => h(InputModel, hData()),
        radio: () => h(RadioModel, hData()),
        select: () => h(SelectModel, hData()),
        selectV2: () => h(SelectV2Model, hData()),
        timePicker: () => h(TimePickerModel, hData())
      }
      return modeType[
        arr.component as
          | 'cascader'
          | 'colorPicker'
          | 'dataPicker'
          | 'operation'
          | 'input'
          | 'radio'
          | 'select'
          | 'selectV2'
          | 'timePicker'
      ]
    }
    return modeTypeChooseFn(props.filterItem)
  }
})

export default formTypeProcessing
