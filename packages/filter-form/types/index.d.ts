import { VNode } from 'vue'

export type IForm = {
  [x: string]: boolean | string | number | Date | string[] | number[] | Date[] | never[]
}

interface IEl {
  [x: string]: any
}

interface ISlot {
  slotName: string
  render: <T>(scope?: T) => VNode
}

export interface ITypeSetting {
  vModel?: string
  component:
    | 'cascader'
    | 'colorPicker'
    | 'dataPicker'
    | 'operation'
    | 'input'
    | 'radio'
    | 'select'
    | 'selectV2'
    | 'timePicker'
    | TRender
  col: IEl
  elFormItem?: {
    slotScope?: ISlot[]
  } & IEl
  customClassName?: {
    elFormItemClassName?: string
    elModuleClassName?: string
  }
  elModule?: {
    slotScope?: ISlot[]
  } & IEl
  infinite?: IEl
  elOption?: IEl
  operationList?: TRender[]
}

export interface IFormConfigurationItems {
  elForm?: IEl
  typeSetting: (ITypeSetting[] | TRender)[]
}

type TRender = (
  val?: JSX.Element | VNode | string | number | null | ((val: any) => any)
) => JSX.Element | VNode | string | number | null | void | boolean
