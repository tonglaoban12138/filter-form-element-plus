import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import { visualizer } from 'rollup-plugin-visualizer'
import copy from 'rollup-plugin-copy'

export default defineConfig({
  plugins: [
    vue(),
    visualizer({
      gzipSize: true,
      brotliSize: true,
      emitFile: false,
      filename: 'test.html',
      open: false
    })
  ],
  build: {
    outDir: 'lib',
    lib: {
      entry: resolve(__dirname, 'packages/filter-form/index.ts'),
      name: 'FilterFormElementPlus',
      fileName: 'filter-form-element-plus',
      formats: ['es', 'umd']
    },
    rollupOptions: {
      external: ['element-plus', 'vue', 'lodash-es'],
      output: {
        globals: { vue: 'Vue', 'lodash-es': 'lodashEs', 'element-plus': 'elementPlus' }
      },
      plugins: [
        copy({
          targets: [
            { src: './packages/filter-form/types/index.d.ts', dest: './lib/types' } //执行拷贝
          ],
          hook: 'closeBundle', // 钩子，插件运行在rollup完成打包并将文件写入磁盘之前
          verbose: true // 在终端进行console.log
        })
      ]
    }
  }
})
